package com.letsignit

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class SimpleAgentSimulation extends Simulation {

  val httpConf = http
    .baseURL("https://cloud-preprod.letsignit.com") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val scn = scenario("get signature") // A scenario is a chain of requests and pauses
    .exec(auth.run).exec(agent.run)


  setUp(scn.inject(
    rampUsersPerSec(1) to 15 during (7 minutes)
  ).protocols(httpConf))

  object auth {

    val credential = StringBody("""{ "email": "robert@silve.net", "password": "azertyuiop" }""")

    val run = http("request_auth")
      .post("/authentication/email/login")
      .body(credential).asJSON
  }

  object agent {
    val run = http("request_signature").get("/api/users/robert@silve.net/signatures/active")
  }

  /*
  object upload {
    val run = http("request_upload")
      .post("/api/images")
      .formUpload("file", "bodies/test.txt")
  }
  */

}

